import FormLogin from '@/app/login/components/FormLogin'

export default function Login(){
  return(
    <div className = 'w-full h-full min-h-screen  bg-white text-black'>
      <div className = 'flex justify-center items-center min-h-screen'>
        <FormLogin/>
      </div>
    </div>
  )
}