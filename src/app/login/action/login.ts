"use server"

import { cookies } from "next/headers"
import moment from 'moment'

interface Login{
  username : string,
  password : string
}

export async function login(credentials:Login){

  try {
    const res = await fetch('https://fakestoreapi.com/auth/login',{
      method : 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body : JSON.stringify({
        username : credentials.username,
        password : credentials.password 
      })
    })
    const data = await res.json()
  
    cookies().set(`token`, data.token,{
      secure:false, 
      expires: moment(new Date()).add(7, "days").toDate(), 
      httpOnly: true,
    }) 

    cookies().set(`username`, credentials.username,{
      secure:false, 
      expires: moment(new Date()).add(7, "days").toDate(), 
      httpOnly: true,
    }) 


    const getAllUser = await fetch('https://fakestoreapi.com/users',{
      method : "GET"
    })

    const dataUser = await getAllUser.json()

    for(const item of dataUser){
      if(credentials.username == item.username){
        cookies().set(`user_id`, item.id,{
          secure:false, 
          expires: moment(new Date()).add(7, "days").toDate(), 
          httpOnly: true,
        }) 
      }
    }


    return data

  } catch (error) {
    let data = {
      code : 400,
      message : "Username dan password tidak valid"
    }

    return data
    
  }


} 

export async function logout(){
  cookies().delete('token')
}


