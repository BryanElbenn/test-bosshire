'use client'
import { useState } from "react"
import { login } from "../action/login"
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { useRouter } from "next/navigation";

interface credentials{
  username : string,
  password :  string,
}

export default function FormLogin(){

  const[username,setUsername] = useState("")
  const[password,setPassword] = useState("")

  const router = useRouter()


  const Login = async() => {

    let credentials:credentials = {
      username : username,
      password : password
    }

    const responseLogin = await login(credentials)

    if(credentials.username.length <= 3){
      toast.error("Username harus lebih panjang dari 3 huruf")
      return
    }

    if(responseLogin.code == 400){
      toast.error(responseLogin.message)
      return
    }

    toast.success("Berhasil Login")

    setTimeout(() => {
      router.replace('/user/products?pagination=1')
    }, 1000);
    
    

  }

  return(
  <div className = 'border border-none shadow-lg w-96 h-96 rounded-md p-10 space-y-5'>
    <ToastContainer/>
    <div className = 'text-xl font-semibold'>
      <p>Welcome!</p>
    </div>
    <div>
      <div>
        <label className = 'text-md'>Username</label>
      </div>
      <div>
        <input type="text" className = 'w-full border px-2 py-2 rounded-md'  onChange={(e)=>{setUsername(e.target.value)}}/>
      </div>

    </div>
    <div>
      <div>
        <label className = 'text-md'>Password</label>
      </div>
      <div>
        <input type="password" className = 'w-full border px-2 py-2 rounded-md' onChange = {(e)=>{setPassword(e.target.value)}}/>
      </div>

    </div>

    <div className = ''>
      <button className = 'w-full px-2 py-2 border rounded-md bg-blue-500 text-white hover:bg-blue-300' onClick={()=>Login()}>Login</button>
    </div>

  </div>
  )
}