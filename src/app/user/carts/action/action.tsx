"use server"
import { cookies } from "next/headers";
import moment from "moment";

export async function getUserById(id:number){

  const res = await fetch(`https://fakestoreapi.com/users/${id}`,{
    method : "GET"
  })

  const data = await res.json();
  return data

}


export async function addNewProduct(cart:Object[]){
  
  try {
  const userId = cookies().get("user_id")

  const res = await fetch('https://fakestoreapi.com/carts',{
    method:"POST",
    headers: {
      'Content-Type': 'application/json',
    },
    body:JSON.stringify(
      {
        userId:userId,
        date:moment(new Date()).format('DD-MM-YYYY'),
        products: cart
      }
    )
    })

  const data = await res.json()
  return data

  } catch (error) {
    let data = {
      code : 400,
      message : "Gagal insert Cart"
    }

    return data
    
  }

}