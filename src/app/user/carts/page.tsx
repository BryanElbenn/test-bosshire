import FilterTable from "./components/FilterTable"


async function getAllCart(){
  const res = await fetch(`https://fakestoreapi.com/carts`,{
    method:"GET"
  })

  const data = await res.json()
  return data
}
async function getAllProducts(){
  const res = await fetch('https://fakestoreapi.com/products',{
    method : "GET",
    cache : "no-cache"
  })

  const data = await res.json();

  return data
}

async function getMappingData(dataCart:any){

  const responseUsername = await fetch(`https://fakestoreapi.com/users`,{
    method:"GET"
  })

  const dataUsername = await responseUsername.json()

  let arrayMappingData = []

  for(const cart of dataCart){
    for(const user of dataUsername){
      if(cart.userId == user.id){
        
        let arrayProduct = []

        for(const product of cart.products){
          const getSingleProduct = await fetch(`https://fakestoreapi.com/products/${product.productId}`,{
            method:"GET"
          })

          const dataProduct = await getSingleProduct.json()
          arrayProduct.push(dataProduct)
        }

        let object = {
          id : cart.id,
          userId : cart.userId,
          username : user.name.firstname+" "+user.name.lastname,
          createdAt : cart.date,
          productList : arrayProduct
        }

        arrayMappingData.push(object)
      }
    }
  }

  return arrayMappingData

}


export default async function Carts({searchParams}:any){

  let {pagination} = searchParams

  const dataCart = await getAllCart()
  const dataMapping = await getMappingData(dataCart)
  const dataProduct = await getAllProducts()

  return(
    <div className = 'px-10 py-20 w-full h-full min-h-screen bg-white text-black space-y-5'>
      <FilterTable dataMapping = {dataMapping} dataProducts = {dataProduct} pagination = {pagination}/>
    </div>
  )
}