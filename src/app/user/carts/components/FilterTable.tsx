'use client'
import moment from "moment"
import {useEffect, useState} from 'react'
import Modal from '@/app/user/carts/components/Modal'
import ModalAllProduct from '@/app/user/carts/components/ModalAllProduct'
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { useRouter } from "next/navigation"


interface props{
  dataMapping : Object[]
  dataProducts : Object[]
  pagination : number
}

export default function FilterTable({dataMapping,dataProducts,pagination}:props){

  const [data,setData] = useState(dataMapping)
  const chunks: any[][] = [];

  const [showModal,setShowModal] = useState<boolean>(false)
  const [showModalAllProduct,setShowModalAllProduct] = useState<boolean>(false)
  const [dataProduct,setDataProduct] = useState([])

  const router = useRouter()

  const [inputProduct,setInputProduct] = useState<Array<Object>>([])

  useEffect(()=>{
    setData([...dataMapping])
  },[dataMapping])

  
  const arr = dataMapping
  const chunkSize = 5;
  for (let i = 0; i < arr.length; i += chunkSize) {
    const chunk = arr.slice(i, i + chunkSize);
    chunks.push(chunk);
  }  


  let arrayData = typeof pagination == 'undefined' ? dataMapping : chunks[pagination-1]

  const modalShow = () =>{
    setShowModal(true)
  }

  const modalAllProductShow = () => {
    setShowModalAllProduct(true)
  }

  const dataModal = (dataProduct:any) =>{
    setDataProduct(dataProduct)
  }
  useEffect(()=>{

    if(dataProduct.length > 0){
      modalShow()
    }


  },[dataProduct])

  const clickPagination = (index:any) =>{
    router.push(`/user/carts/?pagination=${index+1}`)
  }


  return(
    <div className = 'w-full h-full space-y-10'>
      <ToastContainer/>
      <div className = 'flex justify-between'>
        <div>
          <p className = 'text-xl font-bold'>Carts Page</p>
          <p className = 'text-sm text-gray-400'>ini page untuk mengatur carts</p>
        </div>
        <div>
          <button className = 'px-5 py-2 border bg-blue-500 text-white rounded-md hover:bg-blue-300' onClick={()=>modalAllProductShow()}>+ Add Cart</button>
        </div>
      </div>
      <div className = {`${showModal ? "block" : "hidden"}`}>
        <Modal setShowModal = {setShowModal} dataProducts = {dataProduct}/>
      </div>
      <div className = {`${showModalAllProduct ? "block" : "hidden"}`}>
        <ModalAllProduct setShowModalAllProduct = {setShowModalAllProduct} dataProducts = {dataProducts} setInputProduct = {setInputProduct}/>
      </div>

      <div className="overflow-x-auto rounded-md">
        <table className="table-auto border-collapse w-full">
          <thead>
            <tr className="bg-gray-100">
              <th className="px-4 py-2">Cart Belongs to</th>
              <th className="px-4 py-2">Created at</th>
              <th className="px-4 py-2">Action</th>
            </tr>
          </thead>
          <tbody className = 'text-center'>
            {arrayData?.map((data:any,index:number)=>{

              return(
              <tr className="bg-white" key = {index}>
                <td className="border px-4 py-2">{data.username}</td>
                <td className="border px-4 py-2">{moment(data.date).format('MM-DD-YYYY')}</td>
                <td className="border px-4 py-2">
                  <button className = 'px-2 py-2 w-full border bg-blue-500 rounded-md text-white hover:bg-blue-300' onClick={()=>{dataModal(data.productList)}}>Show User Product</button>
                </td>
              </tr>
              )
            })}
      
          </tbody>
        </table>
      </div>

      <div className = 'flex justify-center space-x-3'>
        {chunks.map((data,index)=>{
          return(
            <button className = 'px-2 py-1 border bg-blue-500 text-white' key = {index} onClick={()=>{clickPagination(index)}}>{index+1}</button>
          )
        })}
   
      </div>

    </div>

  )
}