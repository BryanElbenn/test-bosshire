

export default function ButtonAddCart(){
  return(
    <button className = 'px-5 py-2 border bg-blue-500 text-white rounded-md hover:bg-blue-300'>+ Add Cart</button>
  )
}