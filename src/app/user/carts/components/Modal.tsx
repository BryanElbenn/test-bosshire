import { BackdropProps } from "@mui/material"
import { Dispatch, SetStateAction } from "react"
import Image from 'next/image'



interface props{
  setShowModal : Dispatch<SetStateAction<boolean>>
  dataProducts : Object[]
}


export default function Modal({setShowModal,dataProducts}:props){

  return(

    <div className = ''>
    <div
      id="default-modal"
      tab-index="-1"
      aria-hidden="true"
      className="bg-gray-700 bg-opacity-30 mt-10 ml-20 overflow-y-auto overflow-x-hidden fixed flex justify-center items-center z-50 w-full md:inset-0 h-[calc(100%-1rem)] "
    >
      <div className="relative p-4 w-full max-w-2xl max-h-full ">
        <div className="relative bg-white rounded-lg shadow max-h-96 overflow-hidden overflow-y-scroll">
          <div className="flex items-center justify-between p-4 md:p-5 border-b rounded-t">
            <h3 className="text-xl font-semibold ">Show Product</h3>
            <button
              onClick={() => setShowModal(false)}
              type="button"
              className="text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm w-8 h-8 ms-auto inline-flex justify-center items-center dark:hover:bg-gray-600 dark:hover:text-white"
              data-modal-hide="default-modal"
            >
              <svg
                className="w-3 h-3"
                aria-hidden="true"
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 14 14"
              >
                <path
                  stroke="currentColor"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="2"
                  d="m1 1 6 6m0 0 6 6M7 7l6-6M7 7l-6 6"
                />
              </svg>
              <span className="sr-only">Close modal</span>
            </button>
          </div>

          <div className="p-4 md:p-5 space-y-4">
            <p className = 'text-lg'>Data Product</p>
            <div className="w-full  rounded-lg ">
              <div className="w-full space-y-10">
                {dataProducts.map((data:any,index:number)=>{
                  return(
                    <div className = 'border rounded-md p-5 ' key = {index}>
                      <div className = 'flex flex-row items-center space-x-10'>
                        <div>
                          <Image
                            src={data.image}
                            width={150}
                            height={150}
                            alt="Picture of the author"
                          />
                        </div>
    
                        <div>
                          <p>title : {data.title}</p>
                          <p>category : {data.category}</p>
                          <p>price : {data.price}</p>
                        </div>
    
                      </div>
                    </div>
                  )
                })}
            
              </div>
            </div>

          </div>

        </div>
      </div>
    </div>
</div>

  )
}