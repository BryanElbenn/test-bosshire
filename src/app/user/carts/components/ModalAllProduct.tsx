'use client'
import { BackdropProps } from "@mui/material"
import { Dispatch, SetStateAction, useEffect } from "react"
import Image from 'next/image'
import {useState} from 'react'
import { addNewProduct } from "../action/action"
import moment from "moment"
import { toast } from 'react-toastify';
import { useRouter } from "next/navigation"




interface props{
  setShowModalAllProduct : Dispatch<SetStateAction<boolean>>
  dataProducts : Object[]
  setInputProduct : Dispatch<SetStateAction<Array<Object>>>
}


export default function ModalAllProduct({setShowModalAllProduct,dataProducts}:props){

  const router = useRouter()

  const [quantity,setQuantity] = useState<any>({})
  const [cart,setCart] = useState<Object>({})

  const [checkedValue, setCheckedValue] = useState<Array<Object>>([])
 
  const[disableField,setDisableField] = useState<any>({})


  const addCheckedValue = (e:any,idProduct:number,index:number) => {

    let objectProducts = {
      productId : idProduct,
      quantity : typeof quantity[index] == 'undefined' ? 0 : quantity[index] 
    }

    if(e.target.checked){
      setCheckedValue([...checkedValue,objectProducts])
      setDisableField({
        [index] : true,
      })
      return
    }

    setDisableField({
      [index] : false,
    })

    let arr = checkedValue
    arr.splice(index,1)
    setCheckedValue([...arr])
    

  }


  const addToCart = async() => {

    const responseAddNewProduct = await addNewProduct(checkedValue)

    if(responseAddNewProduct.code == 400){
      toast.error(responseAddNewProduct.message)
      return
    }

    toast.success("Berhasil add to cart!")
    setShowModalAllProduct(false)

    router.refresh()

  }


  return(

    <div className = ''>
    <div
      id="default-modal"
      tab-index="-1"
      aria-hidden="true"
      className="bg-gray-700 bg-opacity-30 mt-10 ml-20 overflow-y-auto overflow-x-hidden fixed flex justify-center items-center z-50 w-full md:inset-0 h-[calc(100%-1rem)] "
    >
      <div className="relative p-4 w-full max-w-2xl max-h-full ">
        <div className="relative bg-white w-full rounded-lg shadow max-h-96 overflow-hidden overflow-y-scroll">
          <div className="flex items-center justify-between p-4 md:p-5 border-b rounded-t">
            <h3 className="text-xl font-semibold ">Show Product</h3>
            <button
              onClick={() => setShowModalAllProduct(false)}
              type="button"
              className="text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm w-8 h-8 ms-auto inline-flex justify-center items-center dark:hover:bg-gray-600 dark:hover:text-white"
              data-modal-hide="default-modal"
            >
              <svg
                className="w-3 h-3"
                aria-hidden="true"
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 14 14"
              >
                <path
                  stroke="currentColor"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="2"
                  d="m1 1 6 6m0 0 6 6M7 7l6-6M7 7l-6 6"
                />
              </svg>
              <span className="sr-only">Close modal</span>
            </button>
          </div>

          <div className="p-4 md:p-5 space-y-4">
            <p className = 'text-lg'>Data Product</p>
            <div className="w-full  rounded-lg ">
              <div className="w-full space-y-10">
                {dataProducts.map((data:any,index:number)=>{
                  return(
                    <div className = 'border rounded-md p-5 space-y-5' key = {index}>
                      <div className = 'flex flex-row items-center space-x-10 '>
                        <div>
                          <Image
                            src={data.image}
                            width={150}
                            height={150}
                            alt="Picture of the author"
                          />
                        </div>
    
                        <div>
                          <p>title : {data.title}</p>
                          <p>category : {data.category}</p>
                          <p>price : {data.price}</p>
                        </div>

                      </div>
                      <div>
                        <div>
                          <label htmlFor="">Quantity</label>
                        </div>
                        <div className = 'w-full space-x-2 flex'>
                          <input type="text" className = 'w-full px-2 py-2 rounded-md border ' disabled = {disableField[index]} onChange = {(e)=>{setQuantity({...quantity,[index] : e.target.value})}}/>
                          <input type="checkbox" onClick = {(e)=>{addCheckedValue(e,data.id,index)}}/>
                        </div>
                      </div>
                    </div>
                  )
                })}
            
              </div>
            </div>

          </div>



        </div>
        <div className = 'w-full fixed bottom-10 z-50 flex'>
          <button className = 'px-2 py-2 bg-blue-500 hover:bg-blue-300 rounded-md text-white' onClick = {()=>{addToCart()}}>Add to cart</button>
        </div>
      </div>
    </div>
</div>

  )
}