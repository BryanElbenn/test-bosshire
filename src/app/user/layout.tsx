"use server"
import ResponsiveDrawer from "@/app/user/components/Drawer";
import { cookies } from "next/headers";
import { redirect } from "next/navigation";
import moment from "moment";


async function getCookies(){

  const token = cookies().get("token")
  return token
}


async function getUserData(){
  const res = await fetch(`https://fakestoreapi.com/users`,{
    method : "GET"
  })

  const data = await res.json()
  const objectUser = cookies().get("username")

  const username = objectUser?.value


  let newData = []

  for(const item of data){
    if(item.username == username){
      newData.push(item)
    }
  }

  cookies().set(`user`, newData[0].username,{
    secure:false, 
    expires: moment(new Date()).add(7, "days").toDate(), 
    httpOnly: true,
  }) 

  cookies().set(`user_id`, newData[0].id,{
    secure:false, 
    expires: moment(new Date()).add(7, "days").toDate(), 
    httpOnly: true,
  }) 

  

}



export default async function UserLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {

  const cookies = await getCookies()

  if(typeof cookies == 'undefined'){
    redirect('/login')
  }

  return (
    <html lang="en">
      <body> 
        <ResponsiveDrawer children={children} />
      </body>
    </html>
  );
}