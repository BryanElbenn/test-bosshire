"use server"

export async function deleteItem(id:number){


  try {
    const res = await fetch(`https://fakestoreapi.com/products/${id}`,{
      method : "GET"
    })
    const data = await res.json();
  
    return data

  } catch (error) {
    let data = {
      code : 400,
      message : "Delete gagal"
    }

    return data
    
  }

}
