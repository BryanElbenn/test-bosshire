'use client'
import React,{ useState,useEffect } from "react"
import { useRouter } from "next/navigation"

interface categories{
  category : string[]
  param : string
}

export default function FilterSearch({category,param}: categories){


  const router = useRouter()
  const [chooseCategory,setChooseCategory] = useState(param)

  const selectCategory = (e:React.ChangeEvent<HTMLInputElement>) => {
    setChooseCategory(e.target.value)
  }

  const handleSearch = () => {

    if(chooseCategory == 'all'){
      router.push(`products/?pagination=1`)
      return 
    }

    router.push(`products/?pagination=1&category=${chooseCategory}`)

 
  }


  return(
    <div className = 'w-full space-y-1'>

      <div>
        <label className = 'text-lg font-semibold'>Categories</label>
      </div>

      <div className = 'w-full flex space-x-2'>
        <select name="" id="" value = {chooseCategory} className = 'px-2 py-2 border rounded-md w-full' onChange = {(e:React.ChangeEvent<any>)=>{selectCategory(e)}}>
          <option value="all">All</option>
          {category?.map((data,index)=>{
            return(
              <option value={data} key = {index}>{data}</option>
            )
          })}
        </select>
        <button className = 'px-5 py-2 border rounded-md bg-blue-500 text-white hover:bg-blue-300' onClick={()=>{handleSearch()}}>Search</button>
      </div>

    </div>  
  )
}