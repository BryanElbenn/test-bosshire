'use client'
import { useEffect } from "react";
import { deleteItem } from "../action/action"
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

interface props{
  id : number
  products : Object[]
  setDataProduct : any
  index : number
}

export default function DeleteButton({id,products,setDataProduct,index}:props){

  const fakeDelete = async(id:number,index:number) =>{
    const responseDelete = await deleteItem(id)

    if(responseDelete.code == 400){
      toast.error(responseDelete.message)
      return
    }

    console.log(responseDelete)

    let arr = products 
    arr.splice(index,1)
    setDataProduct([...arr])

    toast.success("Berhasil delete product")
  }

  useEffect(()=>{
    console.log(products)
  },[products])

  return(
    <div className = 'w-full p-5 absolute bottom-0 text-sm'>
      <button className = 'w-full px-2 py-2 border rounded-md bg-red-600 hover:bg-red-300 text-white' onClick={()=>{fakeDelete(id,index)}}>Delete</button>
    </div>
  )
}