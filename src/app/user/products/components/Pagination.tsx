'use client'
import {useRouter} from 'next/navigation'
import {useSearchParams} from 'next/navigation'


interface props{
  chunk:any
}

export default function Pagination({chunk}:props){

  const router = useRouter()
  const searchParams = useSearchParams()

  
  const paginationClick = (index:number) => {
    const params = new URLSearchParams(searchParams.toString())
    params.set('pagination',(index+1).toString())
    window.history.pushState(null,'',`?${params.toString()}`)

    setTimeout(() => {
      router.refresh()
    }, 200);
    
  }

  return(
    <div className = 'flex justify-center py-10 space-x-2 '>
      {chunk.map((data:string,index:number)=>{
        return(
          <button className = 'border px-3 py-2 bg-blue-500 text-white hover:bg-blue-300' key = {index} onClick={()=>paginationClick(index)}>{index+1}</button>
        )
      })}
    </div>
  )
}