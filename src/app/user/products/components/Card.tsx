"use client"
import Image from 'next/image'
import DeleteButton from '@/app/user/products/components/DeleteButton';
import Pagination from '@/app/user/products/components/Pagination';
import {useEffect, useState} from 'react'
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

interface props{
  arrayDataProducts : Object[]
  pagination : number
}

export default function Card({arrayDataProducts,pagination}:props){

  const [dataProduct,setDataProduct] = useState(arrayDataProducts)
  const chunks: any[][] = [];

  useEffect(()=>{
    setDataProduct([...arrayDataProducts])
  },[arrayDataProducts])

  const arr = dataProduct
  const chunkSize = 6;
  for (let i = 0; i < arr.length; i += chunkSize) {
    const chunk = arr.slice(i, i + chunkSize);
    chunks.push(chunk);
  }  

  let arrayData = typeof pagination == 'undefined' ? arrayDataProducts : chunks[pagination-1]

  return(
    <div className = 'w-full'>
      <ToastContainer/>
      <div className = 'grid grid-cols-3 gap-3'>
      {arrayData?.map((data:any,index:number)=>{
        return(
          <div className = 'w-full relative border rounded-md pb-10' key = {index}>
            <div className = 'p-5 pb-10 space-y-3'>
              <div className ='flex justify-between text-xl font-semibold'>
                <p>{data.category}</p>
                <p>Rate : {data.rating.rate} </p>
              </div>
          
              <div className ='flex justify-center'>
                <Image
                  src={data.image}
                  width={500}
                  height={500}
                  alt="Picture of the author"
                  className = 'w-40 h-40'
                />
              </div>
              <div className = 'space-y-2'>
                <p className = 'font-semibold'>{data.title}</p>
                <p className = 'text-gray-400'>{data.description}</p>
              </div>

              <div className='space-y-2'>
                <p className = 'text-lg font-semibold'>Price : {data.price}</p>
                <p className = 'text-lg font-semibold'>Sold : {data.rating.count} items</p>
              </div>
            </div>

            <DeleteButton id = {data.id} products = {dataProduct} setDataProduct = {setDataProduct} index = {index}/>
        
          </div>
        )
      })}


    </div>
    <div className = 'w-full flex justify-center items-center'>
        <Pagination chunk = {chunks}/>
    </div>
  </div>
  )
}