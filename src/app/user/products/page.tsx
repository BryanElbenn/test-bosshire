import Card from './components/Card';
import FilterSearch from '@/app/user/products/components/FilterSearch'
import Pagination from './components/Pagination';


async function getAllProducts(){
  const res = await fetch('https://fakestoreapi.com/products',{
    method : "GET",
    cache : "no-cache"
  })

  const data = await res.json();

  return data
}

async function getAllCategories(){
  const res = await fetch('https://fakestoreapi.com/products/categories',{
    method : "GET",
  })

  const data = await res.json();

  return data
}

async function getProductByCategory(category:string){
  const res = await fetch(`https://fakestoreapi.com/products/category/${category}`,{
    method : "GET",
    cache : "no-cache"
  })

  const data = await res.json();

  return data
}



export default async function Products({searchParams}:any){

  let {category,pagination} = searchParams
  let dataProducts 

  const dataCategories = await getAllCategories()
  if(typeof category == 'undefined' || category == ''){
    dataProducts = await getAllProducts()
  }

  if(typeof category != 'undefined'){
    dataProducts = await getProductByCategory(category)
  }
  
  return(
    <div className = 'px-10 py-20 w-full h-full min-h-screen bg-white text-black space-y-5'>

      <div>
        <p className = 'text-xl font-bold'>Product Page</p>
        <p className = 'text-sm text-gray-400'>ini page untuk menampil kan product</p>
      </div>
      
      <FilterSearch category = {dataCategories} param = {category}/>
      <Card arrayDataProducts = {dataProducts} pagination = {pagination}/>
    </div>
  )
}